#include <pro/sntp.h>
#include "../include/rtc.h"
#include "../include/InternetConnection.h"
#include <dev/board.h>
#include <dev/debug.h>
#include <sys/timer.h>
#include <arpa/inet.h>
#include <net/route.h>
#include <pro/dhcp.h>
#include <pro/sntp.h>
#include <stdlib.h>
#include "../include/menu.h"
#include "../include/display.h"
#include "../include/keyboard.h"
#include "../include/log.h"
#include <stdio.h>
#include <io.h>
#include <string.h>
#include "settings.h"
#include "../include/realTime.h"
#include <time.h>
#define LOG_MODULE  LOG_MAIN_MODULE
#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

int SyncingState = 0; // 0-> Disabled 1-> Enabled

int timeZone=0; // default for GMT
/*!
 * \brief Sets RTC Time
 *
 * \params the seconds minutes hours and date you want to set. It will be set the next second.
 *
 * \return 0 on success or -1 in case of an error.
 */
int setTime(int sec, int min, int hour, int day, int month, int year)
{
        struct _tm timeToSet;
        timeToSet.tm_sec = sec;
        timeToSet.tm_min = min;
        timeToSet.tm_hour = hour;
        timeToSet.tm_mday = day;
        timeToSet.tm_mon = month;
        timeToSet.tm_year = year;

        if(0==X12RtcSetClock(&timeToSet))
        {
            return 0;
        }
        return -1;
   }

/*!
 * \brief Get date and time from an X12xx hardware clock.
 *
 * \param currentTime Points to a structure that receives the date and time
 *           information.
 *
 * \return 0 on success or -1 in case of an error.
 */
int getTime(struct _tm *currentTime)
{
    if(0==X12RtcGetClock(currentTime))
    {
        NutSleep(100);
        //printf("Time [%02d:%02d:%02d] \n", currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);
        return 0;
    }
    return -1;
}
/*!
 * \brief Set the TimeZone
 *
 * \param currentTime TimeZone in int from GMT so GMT=1 -> 1 and GMT-1 -> -1
 *
 * \return 0 on success or -1 in case of an error.
 */
void setTimeZone(int hourDifference)
{
    printf("\n TIMEZONE SET START");
    tm gmt;
    if(0==getTime(&gmt)) {
        gmt.tm_hour -= timeZone;
        gmt.tm_hour += hourDifference;
        X12RtcSetClock(&gmt);
        timeZone = hourDifference;
    }
    changeSetting("tmz", timeZone);

}

void setTMZ(int tmz)
{
    timeZone=tmz;
}
/*!
 * \brief Set the online time from dutch ntp server.
 *
 * \return 0 on success or -1 in case of an error.
 */
int setOnlineTime()
{


    if(1==SyncingState) {

        time_t ntp_time = 0;
        tm *ntp_datetime;
//        ConnectToInternet();
//        if (NutDhcpIfConfig(DEV_ETHER_NAME, 0, 60000)) {
//            puts("Error: Cannot configure network.");
//        }
        /* Timezone for Germany is GMT-1, so we need to subtract 1 hour = 60*60 seconds */

        uint32_t timeserver = inet_addr("87.195.109.106");
        int i;
        DisplayOnLine(0,"Syncing Time");
        DisplayOnLine(1,"With Server");
        for (i = 0; i < 3; i++) {
            if (NutSNTPGetTime(&timeserver, &ntp_time) == 0) {
                printf("Succesvol opgehaald");
                ntp_datetime = localtime(&ntp_time);
                printf("NTP time is: %02d:%02d:%02d\n", ntp_datetime->tm_hour, ntp_datetime->tm_min,
                       ntp_datetime->tm_sec);

                setTime(ntp_datetime->tm_sec, ntp_datetime->tm_min, ntp_datetime->tm_hour+5+timeZone, ntp_datetime->tm_mday,
                        ntp_datetime->tm_mon, ntp_datetime->tm_year);
                NutSleep(100);
                puts("Done.\n");
                ClearDisplay();
                DisplayTime(ntp_datetime->tm_hour+5+timeZone,ntp_datetime->tm_min);
                return 0;
            } else {

                NutSleep(1000);
                ClearDisplay();
                puts("Failed to retrieve time. Retrying...");
                char error[8] = "Attempt";
                error[8] = i;
//                strcpy(error, "Attempt ");
//                sprintf(error,"Attempt %i",i);
                DisplayOnLine(0,error);
                DisplayOnLine(1,"Retrying..");


            }
        }
        return 1;
    }
    return 1;
}
int max = 23;
void changeTime()
{
    int currentNumber = 0;
    tm *time;
    time = malloc(sizeof(tm));

    if(0==    getTime(time)){ printf("Time Received");}
    ClearDisplay();

    int hour =  0;
    int minute = 0;
    DisplayTime(0,0);
    SyncingState = 0;

    int pressed = 1;
    int timeSet = 0;
    while(0==timeSet)
    {

        if (keypressed() == KEY_RIGHT&&pressed==0)
        {
            currentNumber++;
            updateMax(currentNumber);
            printf("CURRENT NUMBER %i /n \n", currentNumber);
            if(currentNumber>1)
            {
                setTime(00,minute,hour,0,0,0);
                timeOkCommand();
            }
            pressed=1;
        }
        if (keypressed() == KEY_LEFT&&pressed==0)
        {
            if(currentNumber>0)
            {
                currentNumber--;
                printf("CURRENT NUMBER %i /n \n", currentNumber);
            }
            pressed=1;
        }
        if (keypressed() == KEY_OK&&pressed==0)
        {
           setTime(00,minute,hour,0,0,0);
            timeOkCommand();
            pressed=1;
            timeSet = 1;
        }
        if (keypressed() == KEY_ESC&&pressed==0 ){
            timeOkCommand();
            timeSet=1;}
        if(keypressed() == KEY_UP&&pressed==0)
        {
            if(currentNumber == 0) {
                if(hour<24&&hour>=0){
                ++hour;}
                else{hour=0;}

            }
            if(currentNumber == 1){
                if(minute<60&&minute>=0){
                    ++minute;}
                else{minute=0;}
        }
            DisplayTime(hour,minute);
            printf("NEW HOUR %i %i NEW MINUTE /n \n", hour, minute);
            pressed=1;
        }
        if(keypressed()==KEY_DOWN&&pressed==0)
        {
            if(currentNumber == 0) {
                if(hour<24&&hour>=0){
                    --hour;}
                else{hour=23;}

            }
            if(currentNumber == 1){
                if(minute<60&&minute>=0){
                    --minute;}
                else{minute=59;}
            }
            printf("NEW HOUR %i %i NEW MINUTE /n \n", hour, minute);
            DisplayTime(hour,minute);
            pressed=1;
        }

        if(keypressed() == KEY_UNDEFINED)
        {
            pressed = 0;
        }
    }

}
void updateMax(int currentNumber)
{
    if(currentNumber ==0)
        max = 23;
    if(currentNumber ==1)
        max = 59;
}

void timeZoneUp()
{
    if(timeZone<=12){
    timeZone++;}
    else{timeZone= -11;}
    changeTimeZone();
}
void timeZoneDown()
{

    if(timeZone>=-11)
    {timeZone--; }
    else{timeZone = 12;}
    changeTimeZone();
}
int oldTimeZone= -50;

void changeTimeZone()
{

    if(oldTimeZone ==-50){
        oldTimeZone=timeZone ;
        timeZone=0;
        timeZoneUp();
    }

    ClearDisplay();
    char toDisplay[7];
    if(timeZone>0){
       sprintf(toDisplay,"GMT +%i",timeZone);
     }
    else if(timeZone<=0){
        sprintf(toDisplay,"GMT %i",timeZone);}


    DisplayOnLine(0, toDisplay);
    int pressed = 1;
    int timeSet = 0;
    while(0==timeSet) {

        if ((keypressed() == KEY_RIGHT || keypressed() == KEY_UP)&&pressed==0)
        {
            pressed=1;
            timeZoneUp();
            timeSet =1;
        }

        if ((keypressed() == KEY_LEFT|| keypressed() == KEY_DOWN)&&pressed==0)
        {
            pressed=1;
            timeZoneDown();
            timeSet =1;
        }
        if (keypressed() == KEY_ESC&&pressed==0)
        {
            pressed=1;
            timeZone = oldTimeZone;
            timeSet =1;
            timeOkCommand();
        }
        if (keypressed() == KEY_OK&&pressed==0)
        {

            pressed=1;
            timeSet =1;
            setTimeZone(timeZone);
            setTMZ(timeZone);
            printf("\n TIMEZONE SET IS %i",timeZone);
            timeOkCommand();
        }

        if(keypressed() == KEY_UNDEFINED)
        {
             pressed = 0;
        }
    }

}
void enableTimeSync()
{
    changeSetting("tms", 1);
    tm gmt;
    SyncingState = 1;
if(0==    setOnlineTime()){
    timeOkCommand();
    ClearDisplay();
        if(0==getTime(&gmt)){
            DisplayOnLine(0, "Succes");
            NutSleep(300);
    DisplayTime(gmt.tm_hour, gmt.tm_min);
            optionsESCCommand();
        }
        optionsESCCommand();
    }
    else {
        ClearDisplay();
        DisplayOnLine(0, "Time Sync Failed.");
        DisplayOnLine(1, "Please try again");
        NutSleep(3000);

        timeOkCommand();
        menuNext();
        menuNext();
        showSelected();

    }
}
void disableTimeSync()
{changeSetting("tms", 0);
    SyncingState = 0;
    timeOkCommand();
    ClearDisplay();
}
void setTimeSync(int newTimeSync)
{
    printf("TIMESYNC");
    SyncingState = newTimeSync;
//    setOnlineTime();
}


/*!
 * \brief Gets the current day of the week. Returns a value between 0 and 6. (0 being sunday, 1 monday etc)
 * 0 - Sunday, 1 - Monday, 2 - Tuesday, 3 - Wednesday, 4 - Thursday, 5 - Friday, 6 - Saturday
 * \param day     the current day of the month
 * \param month   the current month of the year
 * \param year    the current year
 */
int getWeekday(int day, int month, int year){
    int weekday  = (day += month < 3 ? year-- : year - 2, 23*month/9 + day + 4 + year/4- year/100 + year/400)%7;
    return weekday;
}

int getTimeZone()
{
    return timeZone;
}

