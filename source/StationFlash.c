//
// Created by Sascha on 24-3-2017.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../include/flash.h"
#include "../include/flashSettings.h"
#include "../include/radioStations.h"
#include "../include/display.h"
#include "../include/menu.h"
#include "../include/keyboard.h"

#define  MAX_STATIONS_ON_PAGE      (256/(sizeof(STATION)+sizeof(int)))  //Defining max length of the individual page
#define  MAX_PAGES  0x07            //Last page for streams
#define START_PAGE  0x01           //First Page for streams
#define MAX_STATIONS    MAX_STATIONS_ON_PAGE * (MAX_PAGES-START_PAGE)   //Defining max pages

int AStations = 0;                  //THE AMOUNT OF STATIONS STORED
STATION allStations[MAX_STATIONS];  //The list with all STATIONS

//Sets the Amount of Stations
void setAST(int ast)
{
    printf("AST Set");
    AStations = ast;
    //    changeSetting("ast",AStations);       -> Disbled for testing
}

/*
 * Adds a station to the list.
 * If succesfull returns 0 else returns 1
 */
int addStation(STATION station)
{
    allStations[AStations] = station;
    AStations++;
//    changeSetting("ast",AStations);       -> Disbled for testing
if(0==    saveStations()){
    return 0;}
    return 1;
}
/*
 * Removes last station
 * Sets last setting one back
 * return 0 on succes else error occured
 */
int removeLastStation() {
    AStations--;
//    if(0==changeSetting("ast",AStations)){    -> Disabled for testing
    return 0;
//}
    return 1;
}

/*
 * Adds a stream
 * Param desc   -> description of stream
 * Param url   ->  url of stream    (IPV4 Adress)
 * Param path   ->  path of stream
 * Param port   ->  Port number of stream
 *
 * Returns 0 on succes else error occured
 */
int addStream(char desc, char url, char path, int port)
{
    STATION station ={desc, url, path, port};
    addStation(station);
}


//Returns the pointer to the array of stations
STATION * getStations()
{
    return allStations;
}

/*
 * Brief Saves all Stations over multiple pages
 * Returns 0 on succes else an error occured
 */
int saveStations()
{
    int succes = 0;                             //N
    int pages = (AStations/MAX_STATIONS_ON_PAGE);
    if(AStations%MAX_STATIONS_ON_PAGE!=0){pages++;}
    pages+= START_PAGE;
    int i = START_PAGE;
    int base = 0;
    for( i = START_PAGE; i<pages; i++)
    {
        STATION page[MAX_STATIONS_ON_PAGE];
        int j=0;
        for( j = 0; j<MAX_STATIONS_ON_PAGE; j++)
        {
            page[j]=allStations[base];
            if(0!=At45dbPageWrite(i,page,sizeof(page))){succes =-1;}
            base++;
        }
        base--;
    }
    return succes;
}

/*
 * Brief    Loads all stations from multiple pages
 * Returns 0 on succes else an error occured
 */
int loadStations()
{
    int succes = 0;
//    AStations = getSetting("ast");        -> Disabled for Testing Purposes
    printf("\n Stations Loaded: %i",AStations);
    if(AStations>0 && AStations<MAX_STATIONS)   //Defining that amount is whitin Parameters
    {
        int pages = (AStations/MAX_STATIONS_ON_PAGE);      //Calculating amount of pages
        if(AStations%MAX_STATIONS_ON_PAGE!=0){pages++;}    //Adding a page to correct interger dividence
        pages+= START_PAGE;                                //Adding start page
        int i = START_PAGE;
        int base = 0;                                      //Number of station over all pages
        for( i = START_PAGE; i<pages; i++)
        {
            STATION page[MAX_STATIONS_ON_PAGE];
            int j=0;
            for( j = 0; j<MAX_STATIONS_ON_PAGE; j++)        //Loading each page
            {

                if(0!=At45dbPageRead(i,page,sizeof(page))){succes =-1;}
                allStations[base] = page[j];
                base++;
            }
            base--;
        }
    }

}



void printStations()
{
    int i =0;
    for(i=0; i<AStations;i++)
    {
        printf("\n Station %s",allStations[i].desc);
        printf("\n URL %s",allStations[i].url);
        printf("\n Path %s",allStations[i].path);
        printf("\n Port %i",allStations[i].port);
    }
}
/*
 * Prints a page of memory on the console.
 * Parameter    pgn -> PageNumber in HEX which you would want to print.
 */
void showPage(u_long pgn)
{
    unsigned char *pc = (unsigned char *) malloc(264);  //Location where page will be stored
    int idx;

    At45dbPageRead(pgn, (unsigned char *)pc, 264);
    for(idx = 0; idx < 264; idx++)
    {
        if( 0 == (idx % 32) )
            printf("\n");

        if( isalpha(pc[idx]) || isdigit(pc[idx]) )
        {
            printf("%c  ", pc[idx]);
        }
        else
        {
            printf("%02X ", pc[idx]);
        }
    }
    printf("\n");
    free(pc);
}

/*brief Test the saving and loading of two stations from the flash memory
 * Results will be printed in the LOG.
 *
 * Due to testing there may be printed more stations than normally required.
 */
void testFlashSaving()
{
    STATION s = {"Test", "Radio", "URL",50};
    addStation(s);
    printf("\n Station added printing to proof");
    printStations();
    addStream("TEST 2","192.168.1.1", "/3fm",02);
    printf("\n Station added printing to proof");
    printStations();
    loadStations();
    printStations();
    printf("\n Station removed printing to proof");
    removeLastStation();
    printStations();
    printf("\n Station Loaded printing to proof");
    loadStations();
    printStations();
}

char* createIP()
{
    ClearDisplay();
    char ip[]="000000000000";
    DisplayOnLine(0,ip);
    int currentspot = 0;
    DrawArrow(currentspot);

    int i=0;
    int j=0;
    char * iparray;
    getNumbers(ip);
    char realip[15];
    for(i=0;i<12;i++)
    {

        if(i==3||i==6||i ==9)
            j++;
        realip[j]=ip[i];
    }
    return realip;
}

void getNumbers(char * toDisplay)
{
    int i = (sizeof(toDisplay)/ sizeof(char));
    int maxInt = 9;
    int cs  = 0;
    int cn = 0;
    int ispressed = 1;
    while(1)
    {
        if ((keypressed() == KEY_OK) & (!ispressed)) {
            return;
        }
        if((keypressed()== KEY_ESC)&(!ispressed))
        {
            streamOkCommand();
        }
        if ((ispressed) & (keypressed() == KEY_UNDEFINED)) {
            ispressed = 0;
        }
        if ((keypressed() == KEY_UP) & (!ispressed))
        {
            cn++;
            if(cn>maxInt){cn =0;}
            toDisplay[cs]=cn+'0';
            ClearDisplay();
            DisplayOnLine(0,toDisplay);
            DrawArrow(cs+(15-i));
        }
        if ((keypressed() == KEY_DOWN) & (!ispressed))
        {
            cn--;
            if(cn<0){cn=maxInt;}
            toDisplay[cs]=cn +'0';
            ClearDisplay();
            DisplayOnLine(0,toDisplay);
            DrawArrow(cs+(15-i));
        }
        if ((keypressed() == KEY_LEFT) & (!ispressed))
        {
            cs--;
            if(cs<0){cs=0;}
            ClearDisplay();
            DisplayOnLine(0,toDisplay);
            DrawArrow(cs+(15-i));
        }
        if ((keypressed() == KEY_RIGHT) & (!ispressed))
        {
            cs++;
            if(cs>12){cs=12;}
            ClearDisplay();
            DisplayOnLine(0,toDisplay);
            DrawArrow(cs+(15-i));
        }
    }
}