/* ========================================================================
 * [PROJECT]    SIR100
 * [MODULE]     Display
 * [TITLE]      display source file
 * [FILE]       display.c
 * [VSN]        1.0
 * [CREATED]    26092003
 * [LASTCHNGD]  06102006
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 * [PURPOSE]    contains all interface- and low-level routines to
 *              control the LCD and write characters or strings (menu-items)
 * ======================================================================== */

#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"
#include "LedDisplay.h"
#include "keyboard.h"

/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void LcdWriteByte(u_char, u_char);
static void LcdWriteNibble(u_char, u_char);
static void LcdWaitBusy(void);

/*!
 * \addtogroup Display
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* ����������������������������������������������������������������������� */
/*!
 * \brief control backlight
 */
/* ����������������������������������������������������������������������� */
void LcdBackLight(u_char Mode)
{
    if (Mode==LCD_BACKLIGHT_ON)
    {
        sbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn on backlight
    }

    if (Mode==LCD_BACKLIGHT_OFF)
    {
        cbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn off backlight
    }
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Write a single character on the LCD
 *
 * Writes a single character on the LCD on the current cursor position
 *
 * \param LcdChar character to write
 */
/* ����������������������������������������������������������������������� */
void LcdChar(char MyChar)
{
    LcdWriteByte(WRITE_DATA, MyChar);
}

void LcdLoadCustomCharacters()
{
    u_char charmap[40] = {0b00000, 0b11111, 0b00000, 0b01110, 0b00000, 0b00100, 0b00000, 0b00000,   //internet
                          0b00000, 0b00100, 0b00000, 0b00100, 0b00000, 0b00100, 0b00000, 0b00000,   //no internet
                          0b01110, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b00100, 0b00000,   //alarm
                          0b00001, 0b00011, 0b11111, 0b11111, 0b11111, 0b11111, 0b00011, 0b00001,   //sound
                          0b11100, 0b01000, 0b01000, 0b01011, 0b01100, 0b00100, 0b00100, 0b00011};  //timecorrect
    LcdWriteByte(WRITE_COMMAND, 0x40);

    int ch;
    for(ch = 0; ch < 40; ch++)
    {
        LcdWriteByte(WRITE_DATA, charmap[ch]);
        NutSleep(10);
    }
    LcdWriteByte(WRITE_COMMAND, 0x80);
}

void DrawCustomCharacter(char characterID)
{
    int i;
    i = characterID - '0';
    LcdWriteByte(WRITE_DATA, i);
    NutSleep(10);
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level initialisation function of the LCD-controller
 *
 * Initialise the controller and send the User-Defined Characters to CG-RAM
 * settings: 4-bit interface, cursor invisible and NOT blinking
 *           1 line dislay, 10 dots high characters
 *
 */
/* ����������������������������������������������������������������������� */
void LcdLowLevelInit()
{
    u_char i;

    NutDelay(140);                               // wait for more than 140 ms after Vdd rises to 2.7 V

    for (i=0; i<3; ++i)
    {
        LcdWriteNibble(WRITE_COMMAND, 0x33);      // function set: 8-bit mode; necessary to guarantee that
        NutDelay(4);                              // SIR starts up always in 5x10 dot mode
    }

    LcdWriteNibble(WRITE_COMMAND, 0x22);        // function set: 4-bit mode; necessary because KS0070 doesn't
    NutDelay(1);                                // accept combined 4-bit mode & 5x10 dot mode programming

    //LcdWriteByte(WRITE_COMMAND, 0x24);        // function set: 4-bit mode, 5x10 dot mode, 1-line
    LcdWriteByte(WRITE_COMMAND, 0x28);          // function set: 4-bit mode, 5x7 dot mode, 2-lines
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x0C);          // display ON/OFF: display ON, cursor OFF, blink OFF
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x01);          // display clear
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x06);          // entry mode set: increment mode, entire shift OFF

    LcdWriteByte(WRITE_COMMAND, 0x80);          // DD-RAM address counter (cursor pos) to '0'
}


/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level routine to write a byte to LCD-controller
 *
 * Writes one byte to the LCD-controller (by  calling LcdWriteNibble twice)
 * CtrlState determines if the byte is written to the instruction register
 * or to the data register.
 *
 * \param CtrlState destination: instruction or data
 * \param LcdByte byte to write
 *
 */
/* ����������������������������������������������������������������������� */
static void LcdWriteByte(u_char CtrlState, u_char LcdByte)
{
    LcdWaitBusy();                      // see if the controller is ready to receive next byte
    LcdWriteNibble(CtrlState, LcdByte & 0xF0);
    LcdWriteNibble(CtrlState, LcdByte << 4);

}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level routine to write a nibble to LCD-controller
 *
 * Writes a nibble to the LCD-controller (interface is a 4-bit databus, so
 * only 4 databits can be send at once).
 * The nibble to write is in the upper 4 bits of LcdNibble
 *
 * \param CtrlState destination: instruction or data
 * \param LcdNibble nibble to write (upper 4 bits in this byte
 *
 */
/* ����������������������������������������������������������������������� */
static void LcdWriteNibble(u_char CtrlState, u_char LcdNibble)
{
    outp((inp(LCD_DATA_DDR) & 0x0F) | 0xF0, LCD_DATA_DDR);  // set data-port to output again

    outp((inp(LCD_DATA_PORT) & 0x0F) | (LcdNibble & 0xF0), LCD_DATA_PORT); // prepare databus with nibble to write

    if (CtrlState == WRITE_COMMAND)
    {
        cbi(LCD_RS_PORT, LCD_RS);     // command: RS low
    }
    else
    {
        sbi(LCD_RS_PORT, LCD_RS);     // data: RS high
    }

    sbi(LCD_EN_PORT, LCD_EN);

    asm("nop\n\tnop");                    // small delay

    cbi(LCD_EN_PORT, LCD_EN);
    cbi(LCD_RS_PORT, LCD_RS);
    outp((inp(LCD_DATA_DDR) & 0x0F), LCD_DATA_DDR);           // set upper 4-bits of data-port to input
    outp((inp(LCD_DATA_PORT) & 0x0F) | 0xF0, LCD_DATA_PORT);  // enable pull-ups in data-port
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Low-level routine to see if the controller is ready to receive
 *
 * This routine repeatetly reads the databus and checks if the highest bit (bit 7)
 * has become '0'. If a '0' is detected on bit 7 the function returns.
 *
 */
/* ����������������������������������������������������������������������� */
static void LcdWaitBusy()
{
    u_char Busy = 1;
    u_char LcdStatus = 0;

    cbi (LCD_RS_PORT, LCD_RS);              // select instruction register

    sbi (LCD_RW_PORT, LCD_RW);              // we are going to read

    while (Busy)
    {
        sbi (LCD_EN_PORT, LCD_EN);          // set 'enable' to catch 'Ready'

        asm("nop\n\tnop");                  // small delay
        LcdStatus =  inp(LCD_IN_PORT);      // LcdStatus is used elsewhere in this module as well
        Busy = LcdStatus & 0x80;            // break out of while-loop cause we are ready (b7='0')
    }

    cbi (LCD_EN_PORT, LCD_EN);              // all ctrlpins low
    cbi (LCD_RS_PORT, LCD_RS);
    cbi (LCD_RW_PORT, LCD_RW);              // we are going to write
}

/*!
 * Clear display
 */
void ClearDisplay(){
    LcdWriteByte(WRITE_COMMAND, 0x01);
    NutSleep(100);
}

/*!
 * Go to next line
 */
void NextLine(){
    LcdWriteByte(WRITE_COMMAND, 0xC0);
}

/*!
 * Shift the cursor a number of places to the left
 */
void ShiftLeft(int i){
    int index;
    for(index = 0; index < i; index++) {
        LcdWriteByte(WRITE_COMMAND, 0x10);
        NutDelay(5);
    }
}

/*!
 * Shift the cursor a number of places to the right
 */
void ShiftRight(int i){
    int index;
    for(index = 0; index < i; index++) {
        LcdWriteByte(WRITE_COMMAND, 0x14);
        NutDelay(5);
    }
}

/*!
 * Draw arrow up on a certain number from the current position
 */
void DrawArrow(int i){
    SetCursor(1, i);
    LcdChar('^');
}

/*!
 * Change the 2 coming places to a chosen number, when smaller than 10 there will be placed a 0 in front
 */
void ChangeNumbers(int i){
    if(i<10){
        LcdChar('0');
        LcdChar(48 + i);
        ShiftLeft(2);
    } else if (i < 100 && i >= 10){
        LcdChar(48 + i/10);
        LcdChar(48 + i%10);
        ShiftLeft(2);
    }
}

/*!
 * Displaying a number of characters
 * @param text characters to write
 */
void DisplayOnLine(int line, char *text){
    int hasnext = 1;
    int index  = 0;
    int i;

    NutDelay(50);
    SetCursor(line, 8 - (strlen(text)/2));

    while(hasnext) {
        if(text[index + 1] == '\0') hasnext = 0;
        LcdChar(text[index]);
        index++;
    }
}

/*!
 * Display time on screen
 * @param h hours
 * @param m minutes
 */
void DisplayTime(int h, int m) {
    char line[5];
    sprintf(line, "%02d:%02d", h, m);
    DisplayOnLine(0, line);
}

void DisplayIcons(){

}

/*!
 * Set cursor to position on display
 * @param line first or second line(0 or 1)
 * @param pos position(0 to 15)
 */
void SetCursor(int line, int pos)  // max 16
{
    if((pos > 16 || pos < 0) && (line > 1 || line < 0)) {
        return;
    }

    int commandToWrite = 0x80 + (line * 0x40) + pos;
    LcdWriteByte(WRITE_COMMAND, commandToWrite);
}

/*!
 * Draws the internet correct icon in the upper right corner
 */
void DrawInternetConnectionIcon(){
    SetCursor(0, 15);
    DrawCustomCharacter('0');
}

/*!
 * Draws the internet incorrect icon in the upper right corner
 */
void DrawNoInternetConnectionIcon(){
    SetCursor(0, 15);
    DrawCustomCharacter('1');
}

/*!
 * Draws the alarm on icon in the upper left corner
 */
void DrawAlarmOnIcon(){
    SetCursor(0, 0);
    DrawCustomCharacter('2');
}

/*!
 * makes the upper left corner empty
 * OR
 * removes the alarm on icon
 */
void DrawAlarmOffIcon(){
    SetCursor(0, 0);
    LcdChar(' ');
}

/*!
 * Draws the speaker icon in the right down corner with the
 * given percentage of volume
 *
 * @param soundAmount the volume percentage (0 to 10)
 */
void DrawSoundIcon(int soundAmount){
    SetCursor(1,13);LcdChar(' ');
    SetCursor(1,14);LcdChar(' ');
    SetCursor(1,15);LcdChar(' ');

    if(soundAmount<10){
        SetCursor(1, 14);
        DrawCustomCharacter('3');
        SetCursor(1,15);
        char s = soundAmount + '0';
        LcdChar(s);
    }
    else{
        SetCursor(1, 13);
        DrawCustomCharacter('3');
        SetCursor(1,14);
        LcdChar('1');
        SetCursor(1,15);
        LcdChar('0');
    }
}

void DrawTimeCorrectIcon(){
    SetCursor(1,0);
    DrawCustomCharacter('4');
}
/* ---------- end of module ------------------------------------------------ */

/*@}*/
