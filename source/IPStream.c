//
// Created by bartm on 20-2-2017.
//
#include "../include/IPStream.h"
#include "../include/InternetConnection.h"
#include <stdio.h>
#include <string.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include <sys/timer.h>
#include <vs10xx.h>
#include <sys/bankmem.h>
#define LOG_MODULE  LOG_MAIN_MODULE
#include "log.h"
#include "../include/radioStations.h"

char THISFUCKINGTHING[16] = "NA";

/*!
 * Thread for playing stream.
 * Stop thread by changing playerstate to STOP
 * playerstate becomes STOPPED when buffering is stopped!
 * @param arg as FILE * stream!
 */
THREAD(StreamPlayer, arg){
NutThreadSetPriority(20);
FILE *stream = (FILE *) arg;
if(stream != -1){
PlayMp3StreamBuffered(stream);
playerState = STOPPED;
}
NutThreadExit();
NutThreadDestroy();
for(;;) {
NutSleep(10);
}
}


/*!
 * Method for connecting to ip stream and getting all data!
 * @param address The ip adress of the target server in format 127.0.0.1
 * @param port The port of the target server (int) ( ex: 80 for webserver)
 * @param client Your own IP for the target server (given when connected to the internet!)
 * @param radio_url The target path of the webserver in format /<dir>/<subdir>/<file>   ex: / when no subfolders
 * @return Returns the stream of the target server! 0 when is not connected
 */
FILE * ConnectToIPStream(char * address, int port, char * radio_url){
    // connect to client
    TCPSOCKET * socket = ConnectClient(address,port);
    if(-1 == socket){
        LogMsg_P(LOG_ERR, PSTR("returned broken socket"));
        return -1;
    }
    // OPEN STREAM
    FILE * stream;
    if ((stream = _fdopen((int) socket, "r+b")) == 0) {
        LogMsg_P(LOG_ERR, PSTR("Cant' create stream"));
        return -1;
    }
    // GIVE HEADER TO CONNECTED CLIENT
    fprintf(stream, "GET %s HTTP/1.0\r\n", radio_url);
    fprintf(stream, "Host: %s\r\n", address);
    fprintf(stream, "User-Agent: Ethernut\r\n");
    fprintf(stream, "Accept: */*\r\n");
    fprintf(stream, "Icy-MetaData: 1\r\n");
    fprintf(stream, "Connection: close\r\n\r\n");
    fflush(stream);

    u_char *line;
    u_char *cp;
    int stream_confirmed = 0;
    line = malloc(512); // alocate minimum of 512 bytes for the stream
    while(fgets(line, 512, stream)) {
        cp = strchr(line, '\r');
        if(cp == 0) {
            LogMsg_P(LOG_ERR, PSTR("Can't get header out of stream"));
            continue;
        }
        *cp = 0;
        if(*line == 0) {
            break;
        }
        if(strstr(line, "200 OK")){
            stream_confirmed = 1;
        }
    }
    if(stream_confirmed){
        LogMsg_P(LOG_INFO, PSTR("Stream response OK (stream is working)"));
    }else{
        LogMsg_P(LOG_INFO, PSTR("Stream response ERROR (stream is not working)"));
    }
    free(line); // remove allocated memory out of stack
    LogMsg_P(LOG_INFO, PSTR("Stream loaded!"));
    return stream;
}
/*!
 * Function for connecting to ipstream
 * @param radio STATION object with url/path and port parameters that are used in ConnectToIPStream
 * @return stream as FILE * object
 */
FILE * ConnectToIPStreamRadio(STATION * radio){
    LogMsg_P(LOG_INFO, PSTR("Connecting to radiostaton: %s%s:%i"),radio->url,radio->path,radio->port);
    strcpy(THISFUCKINGTHING, radio->desc);
    return ConnectToIPStream(radio->url, radio->port, radio->path);
}
/*!
 * Stop playing streamplayer by stopping VSPlayer and changing playerstate to STOP!
 */
void StopPlaying(){
    if(playerState == PLAYING) {
        LogMsg_P(LOG_INFO, PSTR("Stopping playing!"));
        playerState = STOP;
        if (VsPlayerStop() || playerState == STOP) {
            NutSleep(100);
        }
    }
}
/*!
 * Creates a thread for playing audiostream!
 * @param stream as FILE *
 */
void StartMultiThreaded(FILE * stream) {
    StopPlaying();
    while(playerState != STOPPED && playerState != NOT_STARTED){
        LogMsg_P(LOG_ERR, PSTR("Player is still running!"));
        NutSleep(100);
    }

    if (-1 != stream) {
        LogMsg_P(LOG_INFO, PSTR("Starting StreamPlayer thread playing stream!"));
        playerState = PLAYING;
        NutThreadCreate("SP", StreamPlayer, stream, 512);
    }else{
        LogMsg_P(LOG_ERR, PSTR("Invalid stream! not starting thread"));
    }
}

/*!
 * \brief Play MP3 buffered stream.
 *
 * \param stream Socket stream to read MP3 data from.
 */
void PlayMp3StreamBuffered(FILE *stream)
{
    LogMsg_P(LOG_INFO, PSTR("Buffered playing MP3 Stream"));
    size_t rbytes = 0;
    char *mp3buf;
    int nrBytesRead = 0;
    unsigned char iflag;

    //
    // Init MP3 buffer. NutSegBuf is een globale, systeem buffer
    //
    if( 0 != NutSegBufInit(8192) )
    {
        // Reset global buffer
        iflag = VsPlayerInterrupts(0);
        NutSegBufReset();
        VsPlayerInterrupts(iflag);
    }
    if( -1 == VsPlayerInit() )
    {
        if( -1 == VsPlayerReset(0) )
        {
            LogMsg_P(LOG_ERR, PSTR("Player could not be initialized"));
        }
    }
    while(playerState != STOP)
    {
        /*
         * Query number of byte available in MP3 buffer.
         */
        iflag = VsPlayerInterrupts(0);
        mp3buf = NutSegBufWriteRequest(&rbytes);
        VsPlayerInterrupts(iflag);
        if( VS_STATUS_RUNNING != VsGetStatus() )
        {
            if( rbytes < 1024 )
            {
                LogMsg_P(LOG_INFO, PSTR("Started kicking player! (really playing audio)"));
                VsPlayerKick();
            }
        }

        while( rbytes && playerState != STOP)
        {
            // Copy rbytes (van 1 byte) van stream naar mp3buf.
            nrBytesRead = fread(mp3buf,1,rbytes,stream);
            if( nrBytesRead > 0 )
            {
                iflag = VsPlayerInterrupts(0);
                mp3buf = NutSegBufWriteCommit(nrBytesRead);
                VsPlayerInterrupts(iflag);
                if( nrBytesRead < rbytes && nrBytesRead < 512 )
                {
                    NutSleep(250);
                }
            }
            else
            {
                break;
            }
            rbytes -= nrBytesRead;

            if( nrBytesRead <= 0 )
            {
                break;
            }
        }
    }
    LogMsg_P(LOG_INFO, PSTR("Player is stopping!"));
}

char * getSelected()
{
    return THISFUCKINGTHING;
}

