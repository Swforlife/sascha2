//
// Created by Sascha on 24-3-2017.
//
#include <stdio.h>

#include "../include/flashSettings.h"
#include "../include/flash.h"
#include "../include/realTime.h"
#include "../include/StationFlash.h"
#include "../include/VolumeHandler.h"

#define  SETTINGS_PAGE      0x00                            //Page where the settings are stored
#define  MAX_SETTINGS       (256/sizeof(SETTINGS_STRUCT))   //Amount of Settings that fits on a page

STORED_SETTINGS stored_settings;                            //Array with all the other settings

#define  POSSIBLE_SETTINGS  4                               //SETTINGS THAT ARE ABLE TO BE SET

//PreDefined Settings. Please order from alphabet Descending.
DEFINED_SETTINGS def_set[POSSIBLE_SETTINGS]=
        {
                {"vlm",SetVolume},
                {"tss",setTimeSync},
                {"tmz",setTMZ},
                {"ast",setAST}
        };

//Initializes settings
//Loads settings when more than zero is found.
//Else reset settings.
//Returns -1 on error.
int checkSettings()
{
    stored_settings.amountofsettings=-1;
    if(0==At45dbPageRead(SETTINGS_PAGE, &stored_settings, sizeof(stored_settings) ))
    {
       printf("\n Found %i settings",stored_settings.amountofsettings) ;
    }
    else{ return -1;}
    if(stored_settings.amountofsettings < 0 || stored_settings.amountofsettings>MAX_SETTINGS)
    {
        printf("\n Invalid Amount %i",stored_settings.amountofsettings);
        stored_settings.amountofsettings=0;

        if(0==At45dbPageWrite(SETTINGS_PAGE, &stored_settings, sizeof(stored_settings)))
        {
            return 0;
        }

        return -1;
    }
    return 0;
}

//Adds a setting. On succes return 0 else error occured.
int addSetting(SETTINGS_STRUCT setting)
{
    stored_settings.stored[stored_settings.amountofsettings]= setting;
    stored_settings.amountofsettings++;
    return saveSettings();
}

//Removes last setting
//Returns 0 on succes
int removeLastSetting()
{
    stored_settings.amountofsettings--;
    return saveSettings();
}

//Writes the Settings to the settings Page
int saveSettings()
{

    if(0==At45dbPageWrite(SETTINGS_PAGE, &stored_settings, sizeof(stored_settings)))
    {
        return 0;
    }
    return -1;
}

//Loads the settings from the stored settings page
int loadSettingsFromFlash()
{
    if(0==At45dbPageRead(SETTINGS_PAGE, &stored_settings, sizeof(stored_settings) ))
    {
        printf("\n Loaded %i Settings",stored_settings.amountofsettings);
        return 0;
    }
    return -1;
}

//Executest methods
int loadSettings()
{
    int i =0;
    for(i=0;i<=stored_settings.amountofsettings;i++)
    {
        int j=0;
        printf("\n Setting %i",i);
        for(j=0; j<=POSSIBLE_SETTINGS;j++)
        {
            if(strcmp(stored_settings.stored[i].name==def_set[j].name)==0)
            {
                printf("\nEXECUTING %s \n",def_set[j].name);
                def_set[j].method(stored_settings.stored[i].value);
                continue;
            }
        }



    }
}

//Changes a setting
//Param name -> Setting to be changed
//Param value -> Value to change it to
int changeSetting(char * name, int value)
{
      int i = 0;
    printf("\n Searching for %s",name);
    for(i=0;i<stored_settings.amountofsettings;i++)
    {
        if(strcmp(stored_settings.stored[i].name,name)>=0)
        {
            printf("Changed %s from %i to %i",name, stored_settings.stored[i].value,value);
            stored_settings.stored[i].value = value;
            saveSettings();
            return 0;
        }
    }
    return -1;
}

//Prints all the settings
void showSettings()
{
    int i=0;
    for(i=0; i<=stored_settings.amountofsettings; i++)
    {
        printf("\n Setting %i of %i named %s has value %i", i, stored_settings.amountofsettings, stored_settings.stored[i].name, stored_settings.stored[i].value);
    }
}

//Returns the Value of inserted setting
//Param name -> name of the setting to be returned
int getSetting(char*name)
{
    int i=0;
    for(i=0; i<stored_settings.amountofsettings;i++)
    {
        if(strcmp(stored_settings.stored[i].name,name)>=0)
        {
            return stored_settings.stored[i].value;
        }
    }
}

//Returns the amount of settings
int getAmountOfSettings()
{
    return stored_settings.amountofsettings;
}

//Deletes all settings
int clearSettings()
{

    stored_settings.amountofsettings=-1;
    saveSettings();
    return At45dbPageErase(SETTINGS_PAGE);
}