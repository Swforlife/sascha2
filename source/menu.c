//
// Created by gijsb on 20-2-2017.
//

#define LOG_MODULE  LOG_MAIN_MODULE
#include <stdio.h>
#include <dev/hd44780.h>
#include <dev/term.h>
#include <string.h>
#include <sys/timer.h>
#include <time.h>
#include "../include/rtc.h"
#include <sys/timer.h>
#include "../include/realTime.h"
#include "menu.h"
#include "log.h"
#include "IPStream.h"
#include "../include/radioStations.h"
#include "display.h"
#include "keyboard.h"
#include "alarm.h"
#include "InternetConnection.h"
#include "LedDisplay.h"



int inMenu, isPressed = 1;

/*!
 * Function for initialising the head and selected item.
 */
void menuInit()
{
    while (headItem != NULL) {
        struct MenuItem *next = headItem->next;
        free(headItem);
        headItem = next;
        NutSleep(10);
    }


    NutSleep(200);
}

/*!
 * Shows current selected item. use it if you select the menu.
 */
void showSelected(){
    ClearDisplay();
    DisplayOnLine(0,selectedItem->commandName);
}

/*!
 * naviagtion function. you can get here via the Ok BUTTON  while the radio is at his main screen with the time.
 */
int showMenu() {

    inMenu = 1;
    if ((keypressed() == KEY_RIGHT) & (!isPressed)) {
        menuNext();
        isPressed = 1;
    }

    if ((keypressed() == KEY_LEFT) & (!isPressed)) {
        menuPrev();
        isPressed = 1;
    }

    if ((keypressed() == KEY_OK) & (!isPressed)) {
        selectedItem->commandOk();
        isPressed = 1;
    }

    if ((keypressed() == KEY_ESC) & (!isPressed)) {
        selectedItem->commandESC();
        isPressed = 1;
    }

    if (keypressed() == KEY_UNDEFINED) {
        isPressed = 0;
    }
    NutSleep(200);
    //NutThreadYield();
    return inMenu;
}

/*!
 * Function for adding new menu items.
 */
void addMenuItem(void (*commandOk)(void), void(*commandESC) (void), char *commandName){
    //assigns data to a new item.
    struct MenuItem *newItem = (struct MenuItem *) malloc(sizeof(struct MenuItem));

    if(NULL == newItem) LogMsg_P(LOG_ERR, PSTR("Out of memory! (HEAP: %i"),NutHeapAvailable());
    else
    {
        //If there is no headItem yet, the new menu item takes it's place
        if(NULL == headItem)
        {
            newItem->commandOk = commandOk;
            newItem->commandESC = commandESC;
            strcpy(newItem->commandName, commandName);

            newItem->next = newItem;
            newItem->prev = newItem;

            headItem = newItem;
            selectedItem = newItem;
            lastItem = newItem;
        }
            //if there is a headItem, the newItem also takes it's place and picks the headitem as it's next.
            //the headitem picks the new menu item as its prev.
        else
        {

            newItem->commandOk = commandOk;
            newItem->commandESC = commandESC;
            strcpy(newItem->commandName, commandName);

            newItem->prev = headItem;
            headItem->next = newItem;

            headItem = newItem;

            headItem->next = lastItem;
            lastItem->prev = headItem;
        }
    }
}

/*!
 * Function for navigating to the next menu item.
 */
void menuNext(){
    selectedItem = selectedItem->next;
    showSelected();
}

/*!
 * Function for navigating to the prev menu item.
 */
void menuPrev(){
    selectedItem = selectedItem->prev;
    showSelected();
}
//function monstrosity
void streamOkCommand(){
    menuInit();
    if(playerState == PLAYING){
        addMenuItem(stopStream, subOptionsESCCommand, "Stop");
        addMenuItem(playStreamSwitch, subOptionsESCCommand, "Switch");
    }else {
        int i;
        for (i = 0; i < (sizeof(stations) / sizeof(STATION)); i++) {
            addMenuItem(playStream, subOptionsESCCommand, stations[i].desc);
        }
    }
    addMenuItem(addStreamOK, subOptionsESCCommand, "Add Stream");
    showSelected();
}

char url[16];
char path[20];
int port;
void addStreamOK()
{
    menuInit();
    addMenuItem(setIP,streamOkCommand,"IP");
    addMenuItem(setPORT,streamOkCommand, "PORT");
    addMenuItem(setPATH, streamOkCommand, "PATH");
    addMenuItem(addStreams,streamOkCommand,"ADD");
    showSelected();
}

void setPORT()
{
    char p[4]="0000";
    getNumbers(p);
    strcpy(port,p);
}

void setPATH()
{

}

void addStreams()
{
    char desc[16];
    sprintf(desc,"Stream %i",getSetting("ast"));
    addStream(desc,url,path,port);
    streamOkCommand();
}

void stopStream(){
    StopPlaying();
    subOptionsESCCommand();
}

void playStream(){
    menuInit();
    char * name = selectedItem->commandName;
    char station_name[16];
    strcpy(station_name, name);

    int i;
    for(i = 0; i < (sizeof(stations)/ sizeof(STATION)); i++){
        if(0 == strcmp(stations[i].desc,station_name)){
            menuInit();
            FILE * stream = ConnectToIPStreamRadio(&stations[i]);
            StartMultiThreaded(stream);
            break;
        }
    }
    subOptionsESCCommand();
}

void playStreamSwitch(){
    StopPlaying();
    streamOkCommand();
}

void timeOkCommand(){
    menuInit();
    addMenuItem(changeTime, subOptionsESCCommand, "Change Time");
    addMenuItem(viewTimeZone, subOptionsESCCommand, "View TimeZone");
    addMenuItem(changeTimeZone, subOptionsESCCommand, "Change TimeZone");
    addMenuItem(onlineTimeOkCommand,subOptionsESCCommand,"Online Time?");
    showSelected();
}

void viewTimeZone()
{
    menuInit();
    int i = getTimeZone();
    printf("TIMEZONE %i",i );
    char line [10];
    if(i>0){        sprintf(line,"GMT + %i",i);    }
    else {sprintf(line,"GMT %i",i);}
    addMenuItem(timeOkCommand, timeOkCommand, line);
                showSelected();
}

void onlineTimeOkCommand()
{
    menuInit();
    addMenuItem(enableTimeSync,timeOkCommand, "YES" );
    addMenuItem(disableTimeSync,timeOkCommand, "NO" );
    showSelected();
}

void optionsESCCommand(){
    tm gmt;
if(0==    getTime(&gmt)){
    DisplayTime(gmt.tm_hour, gmt.tm_min);
}
    inMenu = 0;
}


void subOptionsESCCommand(){
    menuInit();

    addMenuItem(alarmOkCommand,optionsESCCommand, "Alarm");
    addMenuItem(internetOkCommand, optionsESCCommand,"Internet");
    addMenuItem(timeOkCommand, optionsESCCommand,"Time");
    addMenuItem(streamOkCommand, optionsESCCommand, "Stream");

    showSelected();
}


void setIP()
{
    strcpy(url, createIP());
    addStreamOK();
}