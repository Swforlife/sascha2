//
// Created by bartm on 20-2-2017.
//
#include "../include/InternetConnection.h"
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <pro/sntp.h>
#include <io.h>
#define LOG_MODULE  LOG_MAIN_MODULE
#include "log.h"
#include "menu.h"
#define ETH0_BASE	0xC300
#define ETH0_IRQ	5
#define GOOGLE "www.google.com"
u_long tmo = 10000;
#define MY_MAC { 0x00, 0x06, 0x98, 0x30, 0x02, 0x76 }
#define MY_IP "192.168.137.100"
#define MY_MASK "255.255.255.0"

void switchInternet(void);
/*!
 * Method for connecting to the internet
 * - Registering network device named "eth0" to ethernut/os
 * - Asking for dynamic ip from dhcp server
 * Printing and returning dynamic ip
 *  \return character array like string which corresponds to device ip (null when can't connect)
 */
char * ConnectToInternet(){
    LogMsg_P(LOG_INFO, PSTR("Registering network device"));
    if( NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ) )
    {
        LogMsg_P(LOG_ERR, PSTR("Can't register device"));
    }else {
        LogMsg_P(LOG_INFO, PSTR("Device Registered | Connecting..."));

        uint8_t mac[6] = MY_MAC;
        uint32_t ip_addr = inet_addr(MY_IP);
        uint32_t ip_mask = inet_addr(MY_MASK);

        if (NutDhcpIfConfig("eth0", NULL, 10000)){
            if(NutNetIfConfig("eth0", mac, ip_addr, ip_mask)){
                LogMsg_P(LOG_ERR, PSTR("Can't get ip from DHCP"));
            }

        } else {
            LogMsg_P(LOG_INFO, PSTR("Got IP!: %s"), inet_ntoa(confnet.cdn_ip_addr));
            return inet_ntoa(confnet.cdn_ip_addr);
        }

    }
    return "";
}
char * ConnectToInternetStatic(){
    LogMsg_P(LOG_INFO, PSTR("Registering network device"));
    if( NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ) )
    {
        LogMsg_P(LOG_ERR, PSTR("Can't register device"));
    }else {
        LogMsg_P(LOG_INFO, PSTR("Device Registered | Connecting..."));

        uint8_t mac[6] = MY_MAC;
        uint32_t ip_addr = inet_addr(MY_IP);
        uint32_t ip_mask = inet_addr(MY_MASK);

        if(NutNetIfConfig("eth0", mac, ip_addr, ip_mask)){
            LogMsg_P(LOG_ERR, PSTR("Can't get ip from DHCP"));
        }
        LogMsg_P(LOG_INFO, PSTR("Got IP!: %s"), inet_ntoa(confnet.cdn_ip_addr));
        return inet_ntoa(confnet.cdn_ip_addr);
    }


    return "";
}
/*!
 * Function for checking internetConnection
 * @return -1 if connection failed and 0 if connection is succes
 */
int * ConnectedToInternet(){
    return ConnectClient("8.8.8.8", 53);
}


/*!
 * Return stream from socket
 * @param socket object that is connected
 * @return -1 when failed and stream as FILE * when succes
 */
FILE * CreateStream(TCPSOCKET * socket){
    FILE * stream;
    if ((stream = _fdopen((int) socket, "r+b")) == 0) {
        LogMsg_P(LOG_ERR, PSTR("Cant' create stream"));
        return -1;
    }
    return stream;
}

/*!
 * TCP Server for handling responses from HTTP server hosted on port 80
 * \param char * response as the text that the user is shown in response
 */
void TCPServer(char * response) {
    TCPSOCKET * sock = ConnectServer(80);     // Accept connection of client
    SendTextToSocket(sock, response);       //  Send response
    CloseSocket(sock);                   // Close session safely
}

/*!
 * Connect to internet client
 * @param adress The address of the client in ip format like: 127.0.0.1
 * @param port the port of the hosted service like: 80 (for http webserver)
 * @return Returns a TCPSOCKET for reading/writing data, CLOSE WHEN FINISHED!
 */
TCPSOCKET * ConnectClient(char * address, int port){
    TCPSOCKET *sock = NutTcpCreateSocket(); // Create socket

    u_short tcpbufsiz = 8760;
    NutTcpSetSockOpt(sock, SO_RCVBUF, &tcpbufsiz, sizeof(tcpbufsiz));
    //u_short mss = 1460;

    //NutTcpSetSockOpt(sock, TCP_MAXSEG, &mss, sizeof(mss));
    u_long rx_to = 3000;

    NutTcpSetSockOpt(sock, SO_RCVTIMEO, &rx_to, sizeof(rx_to));


    if(NutTcpConnect(sock,inet_addr(address), port) == -1){
        LogMsg_P(LOG_ERR, PSTR("Can't connect to server (socket connect error %i : %s:%i)"), NutTcpError(sock), address, port);

        return -1;
    }
    if(sock == 0){
        LogMsg_P(LOG_ERR, PSTR("Can't connect to server (socket null)"));
    }
    return sock;
}

/*!
 * Method for accepting client connections to server
 * @param port which we accept the connection
 * @return the socket connection
 */
TCPSOCKET * ConnectServer(int port){
    TCPSOCKET *sock = NutTcpCreateSocket(); //  Create socket

    if(sock == 0){
        LogMsg_P(LOG_ERR, PSTR("Can't connect to client (socket null)"));
    }
    return sock;
}

/*!
 * Send text as string (character array) to socket
 * Not working when client is not connected
 * @param socket the TCPSocket created by ConnectClient / Server
 * @param text Character array like string
 */
void SendTextToSocket(TCPSOCKET * socket, char * text){
    if(-1 == NutTcpSend(socket, text, strlen(text))){
        LogMsg_P(LOG_ERR, PSTR("Can't send data to socket!"));
    }
}

/*!
 * Method for safely closing TCP connection
 * @param socket socket the TCPSocket created by ConnectClient / Server
 */
void CloseSocket(TCPSOCKET * socket){
    NutTcpCloseSocket(socket); // close connection
}
/*!
 * Show menu for connecting and testing connection in dynamic menu
 */
void internetOkCommand(){
    menuInit();
    addMenuItem(internetTestCommand,subOptionsESCCommand, "Test internet");
    addMenuItem(internetConnectCommand, subOptionsESCCommand, "Connect");
    addMenuItem(internetIPCommand,subOptionsESCCommand, "IP");
    showSelected();
}
/*!
 * Show given ip address in menu
 */
void internetIPCommand(){
    menuInit();
    addMenuItem(internetOkCommand, internetOkCommand, inet_ntoa(confnet.cdn_ip_addr));
    showSelected();
}

/*!
 * Test connection and show if connection failed or connected succesfully
 */
void internetTestCommand(){
    menuInit();
    char * text = "Not connected";
    int * result = ConnectedToInternet();
    if(*result != -1){
        text = "Connected";
    }
    addMenuItem(internetOkCommand, internetOkCommand, text);
    showSelected();
}
/*!
 * Connect to internet (register device and get ip)
 * Chose between dynamic or static (static ip/mac)
 */
void internetConnectCommand(){
    menuInit();
    addMenuItem(internetDynamicCommand,internetOkCommand, "Dynamic");
    addMenuItem(internetStaticCommand, internetOkCommand, "Static");
    showSelected();
}
/*!
 * Connect to the internet with hardcoded mac and ip (not recommended but used when dynamic failed!)
 */
void internetStaticCommand(){
    menuInit();
    ConnectToInternetStatic();
    addMenuItem(internetOkCommand, internetOkCommand, "Connected");
    showSelected();
}
/*!
 * Connect to the internet with dynamic settings (loaded from ROM ) and static when dynamic failed!
 */
void internetDynamicCommand(){
    menuInit();
    ConnectToInternet();
    addMenuItem(internetOkCommand, internetOkCommand, "Connected");
    showSelected();
}

