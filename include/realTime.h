//
// Created by Sascha on 24-2-2017.
//

#ifndef SASCHA2_REALTIME_H
#define SASCHA2_REALTIME_H
int setTime(int sec, int min, int hour, int day, int month, int year);
int setTime(int ,int,int,int,int,int);
int setOnlineTime(void);
void setTimeZone(int );
void setTMZ(int tmz);
void changeTime(void);
void changeTimeZone(void);
void enableTimeSync(void);
void disableTimeSync(void);
void updateMax(int currentNumber);
void timeZoneDown(void);
void timeZoneUp(void);
int getTimeZone();
void setTimeSync(int newTimeSync);
void setTimeSync(int);
int getWeekday(int, int, int);

#endif //SASCHA2_REALTIME_H
