//
// Created by bartm on 20-2-2017.
//

#ifndef INTERNETRADIOPROJECT_IPSTREAM_H
#define INTERNETRADIOPROJECT_IPSTREAM_H
#include <stdio.h>
#include "radioStations.h"
#include <sys/thread.h>
#define NOT_STARTED 0
#define PLAYING 1
#define STOP 2
#define STOPPED 3
int playerState;



THREAD(StreamPlayer, arg);
THREAD(StreamChecker, arg);
void killAudioStream(void);
THREAD(StreamPlayer, arg);
void StartMultiThreaded(FILE * );
void StopPlaying(void);
FILE * ConnectToIPStream(char * , int , char * );
FILE * ConnectToIPStreamRadio(STATION * );
void PlayMp3StreamUnbuffered(FILE *);
void PlayMp3StreamBuffered(FILE *);

char * getSelected(void);

void StartListenThread(void);
void PlayListener(STATION * radio);

#endif //INTERNETRADIOPROJECT_IPSTREAM_H

