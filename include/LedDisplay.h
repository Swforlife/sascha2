//
// Created by gijsb on 7-3-2017.
//

#ifndef CODE_LEDDISPLAY_H
#define CODE_LEDDISPLAY_H

#define LED_BLINK       0
#define LED_ON          1
#define LED_STOP        2

#include <stdio.h>
#include <string.h>

void blinkOn(void);
void blinkOff(void);

void tenSecOn(void);
void AltButtonHandler(void);

void ledOn(void);
void ledOff(void);

void LedOKCommand(void);
void CheckLight(void);
void backLightBlink(void);
void AltBackLightPush(void);
void StartLedThread(void);

u_char returnBLLS(void);

#endif //CODE_LEDDISPLAY_H
