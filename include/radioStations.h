//
// Created by bartm on 13-3-2017.
//

#ifndef INTERNETRADIOPROJECT_RADIOSTATIONS_H
#define INTERNETRADIOPROJECT_RADIOSTATIONS_H

typedef struct {
    char desc[16];
    char url[16];
    char path[20];
    int port;
} STATION;


static STATION * playing;
static STATION stations[] = {
        {
                "Veronica",
                "213.75.57.116",
                "/VERONICACMP3",
                80
        },
        {
                "Radio 1",
                "145.58.52.145",
                "/radio1-bb-mp3",
                80
        },
        {
                "3FM",
                "145.58.52.145",
                "/3fm-bb-mp3",
                80
        }

};


#endif //INTERNETRADIOPROJECT_RADIOSTATIONS_H
