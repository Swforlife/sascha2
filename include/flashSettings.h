//
// Created by Sascha on 24-3-2017.
//

#ifndef SASCHA2_FLASHSETTINGS_H
#define SASCHA2_FLASHSETTINGS_H
#define  MAX_SETTINGS       (256/sizeof(SETTINGS_STRUCT))
typedef struct
{
    char name[3];
    int value;
} SETTINGS_STRUCT;

typedef struct
{
    char name[3];
    void (*method)(int);
} DEFINED_SETTINGS;

typedef struct
{
    int amountofsettings;
    SETTINGS_STRUCT stored[MAX_SETTINGS];
} STORED_SETTINGS;

int checkSettings();
int addSetting(SETTINGS_STRUCT setting);
int removeLastSetting();
int saveSettings();
int loadSettingsFromFlash();
int loadSettings();
int changeSetting(char * name, int value);
void showSettings();
int getSetting(char*name);
int getAmountOfSettings();
int clearSettings();

#endif //SASCHA2_FLASHSETTINGS_H
