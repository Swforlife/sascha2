//
// Created by gijsb on 27-3-2017.
//

#ifndef CODE_HOSTWEBSITE_H
#define CODE_HOSTWEBSITE_H
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <pro/httpd.h>
#include <string.h>

#define PATH "../radio site/RadioPagina.html"

void sendData(void);
void startWebThread(void);
void closeWebsite(void);
int showParams(FILE *stream);

#endif //CODE_HOSTWEBSITE_H
