//
// Created by gijsb on 20-3-2017.
//

#ifndef CODE_VOLUMEHANDLER_H
#define CODE_VOLUMEHANDLER_H

void VolumeUp(void);
void VolumeDown(void);
void SetVolume(int);

void VolumeInit(void);
void VolumeHandler(void);

void VolumeIconUpdate(void);
int getVolume(void);

#endif //CODE_VOLUMEHANDLER_H
