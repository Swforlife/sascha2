//
// Created by Sascha on 24-3-2017.
//

#ifndef SASCHA2_STATIONFLASH_H
#define SASCHA2_STATIONFLASH_H
#define  MAX_STATIONS_ON_PAGE      (256/(sizeof(STATION)+sizeof(int)))
#define  MAX_PAGES  0x07
#define START_PAGE  0x01
#define MAX_STATIONS    MAX_STATIONS_ON_PAGE * (MAX_PAGES-START_PAGE)
#include "../include/radioStations.h"
int addStation(STATION station);
int removeLastStation();
int addStream(char desc, char url, char path, int port);
STATION * getStations();
void testFlashSaving(void);
char * getNumbers(char * toDisplay);
char* createIP();
int saveStations();
int loadStations();
void setAST(int ast);
void showPage(u_long pgn);
void printStations();
#endif //SASCHA2_STATIONFLASH_H
